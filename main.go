package main

import (
	"flag"
	"log"
	"os"

	"github.com/joho/godotenv"
)

func main() {
	err := godotenv.Load("./config/.env")
	if err != nil {
		log.Printf("error loading .env file %v", err)
	}

	flag.Parse()
	log.Printf("this is input %v", flag.Args())
	log.Printf("env S3_BACKET_NAME=%s", os.Getenv("S3_BACKET_NAME"))
}
