ARG GOLANG_VERSION

FROM registry.gitlab.com/shitomo/go-batch-sample/golang:${GOLANG_VERSION}-bullseye as builder

ENV CGO_ENABLED=0
ENV GOOS=linux
ENV GOARCH=amd64
WORKDIR /ws
COPY . /ws
RUN go build -o main

# runtime image
FROM alpine:latest
RUN apk add --no-cache ca-certificates
WORKDIR /usr/local/
COPY --from=builder /ws/main /main
ENTRYPOINT ["/main"]